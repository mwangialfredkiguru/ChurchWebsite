<footer class="an-footer">
        <p>COPYRIGHT 2018 © ANALOG. ALL RIGHTS RESERVED</p>
      </footer> <!-- end an-footer -->
    </div> <!-- end .MAIN-WRAPPER -->
    <script src="assets/js-plugins/jquery-3.1.1.min.js" type="text/javascript"></script>
    <script src="assets/js-plugins/bootstrap.min.js" type="text/javascript"></script>
    <script src="assets/js-plugins/moment.min.js" type="text/javascript"></script>
    <script src="assets/js-plugins/daterangepicker.js" type="text/javascript"></script>
    <script src="assets/js-plugins/wow.min.js" type="text/javascript"></script>
    <script src="assets/js-plugins/perfect-scrollbar.jquery.min.js" type="text/javascript"></script>
    <script src="assets/js-plugins/selectize.min.js" type="text/javascript"></script>
    <script src="assets/js-plugins/owl.carousel.min.js" type="text/javascript"></script>
    <script src="assets/js-plugins/masonry.pkgd.min.js" type="text/javascript"></script>
    <script src="assets/js-plugins/Chart.min.js" type="text/javascript"></script>
    <script src="assets/js-plugins/circle-progress.min.js" type="text/javascript"></script>
    <!--  MAIN SCRIPTS START FROM HERE  above scripts from plugin   -->
    <script src="assets/js/customize-chart.js" type="text/javascript"></script>
    <script src="assets/js/scripts.js" type="text/javascript"></script>
  </body>

<!-- Mirrored from analoglyf.com/admin/html-template/admin_cool/ by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 14 Jul 2019 09:05:38 GMT -->
</html>
