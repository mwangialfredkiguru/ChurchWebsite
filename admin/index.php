<?php include 'header.php'; ?>


<div class="an-page-content">
<?php include 'nav.php'; ?>

        <div class="an-content-body home-body-content">
          <div class="an-breadcrumb wow fadeInUp">
            <ol class="breadcrumb">
              <li><a href="#">Home</a></li>
              <li><a href="#">Admin Panel</a></li>
              <li class="active">Dashboard</li>
            </ol>
          </div> <!-- end AN-BREADCRUMB -->


          <div class="an-panel-main-info">
            <div class="row">
              <div class="col-md-3 col-sm-6">
                <div class="an-panel-main-info-single color-cyan with-shadow wow fadeIn" data-wow-delay=".1s">
                  <h2>94.9% <span class="info-identifier">Server up time</span></h2>
                  <i class="icon-chart"></i>
                </div>
              </div>
              <div class="col-md-3 col-sm-6">
                <div class="an-panel-main-info-single lime-green with-shadow wow fadeIn" data-wow-delay=".2s">
                  <h2>1,543 <span class="info-identifier">Total users</span></h2>
                  <i class="icon-user"></i>
                </div>
              </div>
              <div class="col-md-3 col-sm-6">
                <div class="an-panel-main-info-single pale-yellow with-shadow wow fadeIn" data-wow-delay=".3s">
                  <h2>$2,000 <span class="info-identifier">Revenue</span></h2>
                  <i class="icon-cart"></i>
                </div>
              </div>
              <div class="col-md-3 col-sm-6">
                <div class="an-panel-main-info-single soft-pink with-shadow wow fadeIn" data-wow-delay=".4s">
                  <h2>+45% <span class="info-identifier">Bookings</span></h2>
                  <i class="icon-book-o"></i>
                </div>
              </div>

            </div> <!-- end .ROW -->
          </div> <!-- end .AN-PANEL-MAIN-INFO -->

          <div class="row an-masonry-layout">
            
            <div class="col-md-3 grid-item">
              
            </div>
            <div class="col-md-12 grid-item">
              <div class="an-single-component with-shadow">
                <div class="an-component-header">
                  <h6>Messages</h6>
                  <div class="component-header-right">
                    <form class="an-form" action="#">
                      <div class="an-search-field">
                        <input class="an-form-control" type="text" placeholder="Search...">
                        <button class="an-btn an-btn-icon" type="submit"><i class="icon-search"></i></button>
                      </div>
                    </form>
                    <div class="an-default-select-wrapper">
                      <select name="sort">
                        <option value="0">Show All</option>
                        <option value="1">Unread</option>
                        <option value="2">Read</option>
                      </select>
                    </div>
                  </div>
                </div>
                <div class="an-component-body">
                  <div class="an-user-lists messages">
                    <div class="list-title">
                      <h6 class="basis-30">
                        <span class="an-custom-checkbox">
                          <input type="checkbox" id="check-1">
                          <label for="check-1"></label>
                        </span>
                        Users
                      </h6>
                      <h6 class="basis-20">Date</h6>
                      <h6 class="basis-30">Text</h6>
                      <h6 class="basis-10">state</h6>
                      <h6 class="basis-10">Action</h6>
                    </div>

                    <div class="an-lists-body an-customScrollbar">
                      <div class="list-user-single">
                        <div class="list-name basis-30">
                          <span class="an-custom-checkbox">
                            <input type="checkbox" id="check-2">
                            <label for="check-2"></label>
                          </span>
                          <span class="image"
                            style="background: url('assets/img/users/user2.jpg') center center no-repeat;
                            background-size: cover;">
                          </span>
                          <a href="#">Alex Jordan</a>
                        </div>
                        <div class="list-date basis-20">
                          <p>15 Nov 2016</p>
                        </div>
                        <div class="list-text basis-30">
                          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit</p>
                        </div>
                        <div class="list-state basis-10">
                          <span class="msg-tag unread">Unread</span>
                        </div>
                        <div class="list-action basis-10">
                          <div class="btn-group">
                            <button type="button" class="an-btn an-btn-icon small dropdown-toggle"
                              data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                              <i class="icon-setting"></i>
                            </button>
                            <div class="dropdown-menu right-align">
                              <ul class="an-basic-list">
                                <li><a href="#">Mark as read</a></li>
                                <li><a href="#">Mark as unread</a></li>
                                <li><a href="#">Select</a></li>
                              </ul>
                            </div>
                          </div>
                          <button class="an-btn an-btn-icon small muted danger"><i class="icon-trash"></i></button>
                        </div>
                      </div> <!-- end .USER-LIST-SINGLE -->

                      <div class="list-user-single">
                        <div class="list-name basis-30">
                          <span class="an-custom-checkbox">
                            <input type="checkbox" id="check-3">
                            <label for="check-3"></label>
                          </span>
                          <span class="image"
                            style="background: url('assets/img/users/user3.jpg') center center no-repeat;
                            background-size: cover;">
                          </span>
                          <a href="#">Sylvain Guiheneuc</a>
                        </div>
                        <div class="list-date basis-20">
                          <p>12 Nov 2016</p>
                        </div>
                        <div class="list-text basis-30">
                          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit</p>
                        </div>
                        <div class="list-state basis-10">
                          <span class="msg-tag read">read</span>
                        </div>
                        <div class="list-action basis-10">
                          <div class="btn-group">
                            <button type="button" class="an-btn an-btn-icon small dropdown-toggle"
                              data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                              <i class="icon-setting"></i>
                            </button>
                            <div class="dropdown-menu right-align">
                              <ul class="an-basic-list">
                                <li><a href="#">Mark as read</a></li>
                                <li><a href="#">Mark as unread</a></li>
                                <li><a href="#">Select</a></li>
                              </ul>
                            </div>
                          </div>
                          <button class="an-btn an-btn-icon small muted danger"><i class="icon-trash"></i></button>
                        </div>
                      </div> <!-- end .USER-LIST-SINGLE -->

                      <div class="list-user-single">
                        <div class="list-name basis-30">
                          <span class="an-custom-checkbox">
                            <input type="checkbox" id="check-4">
                            <label for="check-4"></label>
                          </span>
                          <span class="image"
                            style="background: url('assets/img/users/user4.jpg') center center no-repeat;
                            background-size: cover;">
                          </span>
                          <a href="#">Ales Krivec</a>
                        </div>
                        <div class="list-date basis-20">
                          <p>14 Nov 2016</p>
                        </div>
                        <div class="list-text basis-30">
                          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit</p>
                        </div>
                        <div class="list-state basis-10">
                          <span class="msg-tag draft">draft</span>
                        </div>
                        <div class="list-action basis-10">
                          <div class="btn-group">
                            <button type="button" class="an-btn an-btn-icon small dropdown-toggle"
                              data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                              <i class="icon-setting"></i>
                            </button>
                            <div class="dropdown-menu right-align">
                              <ul class="an-basic-list">
                                <li><a href="#">Mark as read</a></li>
                                <li><a href="#">Mark as unread</a></li>
                                <li><a href="#">Select</a></li>
                              </ul>
                            </div>
                          </div>
                          <button class="an-btn an-btn-icon small muted danger"><i class="icon-trash"></i></button>
                        </div>
                      </div> <!-- end .USER-LIST-SINGLE -->

                      <div class="list-user-single">
                        <div class="list-name basis-30">
                          <span class="an-custom-checkbox">
                            <input type="checkbox" id="check-5">
                            <label for="check-5"></label>
                          </span>
                          <span class="image"
                            style="background: url('assets/img/users/user6.jpg') center center no-repeat;
                            background-size: cover;">
                          </span>
                          <a href="#">Greg Rakozy</a>
                        </div>
                        <div class="list-date basis-20">
                          <p>10 Nov 2016</p>
                        </div>
                        <div class="list-text basis-30">
                          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit</p>
                        </div>
                        <div class="list-state basis-10">
                          <span class="msg-tag spam">spam</span>
                        </div>
                        <div class="list-action basis-10">
                          <div class="btn-group">
                            <button type="button" class="an-btn an-btn-icon small dropdown-toggle"
                              data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                              <i class="icon-setting"></i>
                            </button>
                            <div class="dropdown-menu right-align">
                              <ul class="an-basic-list">
                                <li><a href="#">Mark as read</a></li>
                                <li><a href="#">Mark as unread</a></li>
                                <li><a href="#">Select</a></li>
                              </ul>
                            </div>
                          </div>
                          <button class="an-btn an-btn-icon small muted danger"><i class="icon-trash"></i></button>
                        </div>
                      </div> <!-- end .USER-LIST-SINGLE -->

                    </div> <!-- end .AN-LISTS-BODY -->
                  </div>
                </div> <!-- end .AN-COMPONENT-BODY -->
              </div> <!-- end .AN-SINGLE-COMPONENT messages -->
            </div>

          </div> <!-- end first row -->
        </div> <!-- end .AN-PAGE-CONTENT-BODY -->
      </div> <!-- end .AN-PAGE-CONTENT -->

<?php include 'footer.php'; ?>
