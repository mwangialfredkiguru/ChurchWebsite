<?php 

?>

<div class="an-sidebar-nav js-sidebar-toggle-with-click">

          <div class="an-sidebar-nav">
            <ul class="an-main-nav">
              <li class="an-nav-item">
                <a class="" href="index.php">
                  <i class="icon-chart-stock"></i>
                  <span class="nav-title">Dashboard</span>
                </a>
              </li>

              <li class="an-nav-item">
                <a class="" href="index.php">
                  <i class="icon-squares"></i>
                  <span class="nav-title">Users</span>
                </a>
              </li>

              <li class="an-nav-item">
                <a class="" href="index.php">
                  <i class="icon-book"></i>
                  <span class="nav-title">Offering</span>
                </a>
              </li>
              <li class="an-nav-item">
                <a class="" href="forms.html">
                  <i class="icon-board-list"></i>
                  <span class="nav-title">Tithes</span>
                </a>
              </li>


              <li class="an-nav-item">
                <a class="" href="charts.html">
                  <i class="icon-chart"></i>
                  <span class="nav-title">Charts</span>
                </a>
              </li>

              <li class="an-nav-item">
                <a class="" href="maps.html">
                  <i class="icon-marker"></i>
                  <span class="nav-title">Maps</span>
                </a>
              </li>

              <li class="an-nav-item">
                <a class="" href="inbox.html">
                  <i class="icon-chat-o"></i>
                  <span class="nav-title">Inbox <span class="an-arrow-nav count">3</span></span>
                </a>
              </li>
              <li class="an-nav-item">
                <a class=" js-show-child-nav" href="#">
                  <i class="icon-dot-horizontal"></i>
                  <span class="nav-title">Sidebar layouts
                    <span class="an-arrow-nav"><i class="icon-arrow-down"></i></span>
                  </span>
                </a>
                <ul class="an-child-nav js-open-nav">
                  <li><a href="layout-sidebar-default.html">Default Sidebar</a></li>
                  <li><a href="layout-sidebar-hidden.html">Hidden Sidebar</a></li>
                </ul>
              </li>
            </ul> <!-- end .AN-MAIN-NAV -->
          </div> <!-- end .AN-SIDEBAR-NAV -->
        </div> <!-- end .AN-SIDEBAR-NAV -->