<!DOCTYPE html>
<html lang="zxx">
  
<!-- Mirrored from analoglyf.com/admin/html-template/admin_cool/ by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 14 Jul 2019 09:04:36 GMT -->
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>P.C.E.A | Admin</title>

    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet">
    <link href="assets/css/animate.min.css" rel="stylesheet">

    <link href="assets/css/vendor-styles.css" rel="stylesheet">
    <link rel="stylesheet" href="assets/css/styles.css">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
    <div class="main-wrapper">
    <div class="an-loader-container">
        <img src="assets/img/loader.png" alt="">
      </div>
      <header class="an-header">
        <div class="an-topbar-left-part">
          <h3 class="an-logo-heading">
            <a class="an-logo-link" href="index-2.html">Analog
              <span>Admin Template</span>
            </a>
          </h3>
          <button class="an-btn an-btn-icon toggle-button js-toggle-sidebar">
            <i class="icon-list"></i>
          </button>
          <form class="an-form" action="#">
            <div class="an-search-field topbar">
              <input class="an-form-control" type="text" placeholder="Search...">
              <button class="an-btn an-btn-icon" type="submit">
                <i class="icon-search"></i>
              </button>
            </div>
          </form>
        </div> <!-- end .AN-TOPBAR-LEFT-PART -->

        <div class="an-topbar-right-part">
          <div class="an-notifications">
            <div class="btn-group an-notifications-dropown notifications">
              <button type="button"
                class="an-btn an-btn-icon dropdown-toggle js-has-new-notification"
                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="ion-ios-bell-outline"></i>
              </button>
              <div class="dropdown-menu">
                <p class="an-info-count">Notifications <span>3</span></p>
                <div class="an-info-content notifications-info notifications-content">
                  <div class="an-info-single unread">
                    <a href="#">
                      <span class="icon-container important">
                        <i class="icon-setting"></i>
                      </span>
                      <div class="info-content">
                        <h5 class="user-name">Settings updated</h5>
                        <p class="content"><i class="icon-clock"></i> 30 min ago</p>
                      </div>
                    </a>
                  </div>

                  <div class="an-info-single unread">
                    <a href="#">
                      <span class="icon-container success">
                        <i class="icon-cart"></i>
                      </span>
                      <div class="info-content">
                        <h5 class="user-name">5 Orders placed</h5>
                        <p class="content"><i class="icon-clock"></i> 1 hour ago</p>
                      </div>
                    </a>
                  </div>

                  <div class="an-info-single unread">
                    <a href="#">
                      <span class="icon-container nutral">
                        <i class="icon-chat-o"></i>
                      </span>
                      <div class="info-content">
                        <h5 class="user-name">3 New messages </h5>
                        <p class="content"><i class="icon-clock"></i> 1 hour ago</p>
                      </div>
                    </a>
                  </div>

                  <div class="an-info-single">
                    <a href="#">
                      <span class="icon-container warning">
                        <i class="icon-alerm"></i>
                      </span>
                      <div class="info-content">
                        <h5 class="user-name">This is warning notification</h5>
                        <p class="content"><i class="icon-clock"></i> 1 hour ago</p>
                      </div>
                    </a>
                  </div>

                  <div class="an-info-single">
                    <a href="#">
                      <span class="icon-container danger"><i class="icon-danger"></i></span>
                      <div class="info-content">
                        <h5 class="user-name">Server loaded by 98% please recover soon</h5>
                        <p class="content"><i class="icon-clock"></i> 1 hour ago</p>
                      </div>
                    </a>
                  </div>
                </div> <!-- end .AN-INFO-CONTENT -->
                <div class="an-info-show-all-btn">
                  <a class="an-btn an-btn-transparent fluid rounded uppercase small-font" href="#">Show all</a>
                </div>
              </div>
            </div>
          </div> <!-- end .AN-NOTIFICATION -->

          <div class="an-messages">
            <div class="btn-group an-notifications-dropown messages">
              <button type="button" class="an-btn an-btn-icon dropdown-toggle js-has-new-messages"
                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="ion-ios-email-outline"></i>
              </button>
              <div class="dropdown-menu">
                <p class="an-info-count">Messages <span>3</span></p>
                <div class="an-info-content notifications-info">
                  <div class="an-info-single unread">
                    <a href="#">
                      <span class="user-img" style="background-image: url('assets/img/users/user1.jpg')"></span>
                      <div class="info-content">
                        <h5 class="user-name">Ana malik</h5>
                        <p class="content">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do.</p>
                        <span class="info-time"><i class="icon-clock"></i>15:28</span>
                      </div>
                    </a>
                  </div>

                  <div class="an-info-single unread">
                    <a href="#">
                      <span class="user-img" style="background-image: url('assets/img/users/user2.jpg')"></span>
                      <div class="info-content">
                        <h5 class="user-name">Jackson Fred</h5>
                        <p class="content">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do.</p>
                        <span class="info-time"><i class="icon-clock"></i>4:54</span>
                      </div>
                    </a>
                  </div>

                  <div class="an-info-single">
                    <a href="#">
                      <span class="user-img" style="background-image: url('assets/img/users/user3.jpg')"></span>
                      <div class="info-content">
                        <h5 class="user-name">Emma Watson</h5>
                        <p class="content">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do.</p>
                        <span class="info-time"><i class="icon-clock"></i>28 Sep</span>
                      </div>
                    </a>
                  </div>

                  <div class="an-info-single">
                    <a href="#">
                      <span class="user-img" style="background-image: url('assets/img/users/user4.jpg')"></span>
                      <div class="info-content">
                        <h5 class="user-name">Elina</h5>
                        <p class="content">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do.</p>
                        <span class="info-time"><i class="icon-clock"></i>28 Sep</span>
                      </div>
                    </a>
                  </div>

                  <div class="an-info-single">
                    <a href="#">
                      <span class="user-img" style="background-image: url('assets/img/users/user5.jpg')"></span>
                      <div class="info-content">
                        <h5 class="user-name">Jack Elison</h5>
                        <p class="content">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do.</p>
                        <span class="info-time"><i class="icon-clock"></i>20 Sep</span>
                      </div>
                    </a>
                  </div>

                  <div class="an-info-single">
                    <a href="#">
                      <span class="user-img" style="background-image: url('assets/img/users/user6.jpg')"></span>
                      <div class="info-content">
                        <h5 class="user-name">Lara Smith</h5>
                        <p class="content">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do.</p>
                        <span class="info-time"><i class="icon-clock"></i>10 Sep</span>
                      </div>
                    </a>
                  </div>
                </div> <!-- end .AN-INFO-CONTENT -->

                <div class="an-info-show-all-btn">
                  <a class="an-btn an-btn-transparent fluid rounded uppercase small-font" href="#">Show all</a>
                </div>
              </div>
            </div>
          </div> <!-- end .AN-MESSAGE -->

          <div class="an-profile-settings">
            <div class="btn-group an-notifications-dropown  profile">
              <button type="button" class="an-btn an-btn-icon dropdown-toggle"
                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <span class="an-profile-img" style="background-image: url('assets/img/users/user5.jpg');"></span>
                <span class="an-user-name">John Smith</span>
                <span class="an-arrow-nav"><i class="icon-arrow-down"></i></span>
              </button>
              <div class="dropdown-menu">
                <p class="an-info-count">Profile Settings</p>
                <ul class="an-profile-list">
                  <li><a href="#"><i class="icon-user"></i>My profile</a></li>
                  <li><a href="#"><i class="icon-envelop"></i>My inbox</a></li>
                  <li><a href="#"><i class="icon-calendar-check"></i>Calendar</a></li>
                  <li role="separator" class="divider"></li>
                  <li><a href="#"><i class="icon-lock"></i>Lock screen</a></li>
                  <li><a href="#"><i class="icon-download-left"></i>Log out</a></li>
                </ul>
              </div>
            </div>
          </div> <!-- end .AN-PROFILE-SETTINGS -->
        </div> <!-- end .AN-TOPBAR-RIGHT-PART -->
      </header> <!-- end .AN-HEADER -->