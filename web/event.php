<?php include 'layout/header.php'; ?>
<?php include 'layout/nav.php'; ?>

    <!-- subheader begin -->
    <section id="subheader" data-speed="1" data-type="background">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h1>Events</h1>
                </div>
            </div>
        </div>
    </section>
    <!-- subheader close -->

    <div class="clearfix"></div>

    <!-- content begin -->
    <div id="content">

        <div class="container">
            <div class="row">
                <!-- event item begin -->
                <div class="col-md-6 event-item">
                    <div class="inner">
                        <div class="left-col">
                            <img src="img/events/pic%20(1).jpg" alt="">
                        </div>
                        <div class="right-col">
                            <span class="date">06</span>
                            <span class="month">FEB</span>
                            <span class="time">08:00 am</span>
                        </div>
                    </div>
                    <div class="desc">
                        <a href="#">
                            <h3>Family Baptism Class</h3>
                        </a>
                        <span class="text">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip.
                        </span>
                    </div>
                </div>
                <!-- event item close -->

                <!-- event item begin -->
                <div class="col-md-6 event-item">
                    <div class="inner">
                        <div class="left-col">
                            <img src="img/events/pic%20(2).jpg" alt="">
                        </div>
                        <div class="right-col">
                            <span class="date">10</span>
                            <span class="month">FEB</span>
                            <span class="time">10:00 am</span>
                        </div>
                    </div>
                    <div class="desc">
                        <a href="#">
                            <h3>Transforming Live</h3>
                        </a>
                        <span class="text">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip.
                        </span>
                    </div>
                </div>
                <!-- event item close -->

                <!-- event item begin -->
                <div class="col-md-6 event-item">
                    <div class="inner">
                        <div class="left-col">
                            <img src="img/events/pic%20(3).jpg" alt="">
                        </div>
                        <div class="right-col">
                            <span class="date">20</span>
                            <span class="month">FEB</span>
                            <span class="time">10:00 am</span>
                        </div>
                    </div>
                    <div class="desc">
                        <a href="#">
                            <h3>Relationship With God</h3>
                        </a>
                        <span class="text">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip.
                        </span>
                    </div>
                </div>
                <!-- event item close -->

                <!-- event item begin -->
                <div class="col-md-6 event-item">
                    <div class="inner">
                        <div class="left-col">
                            <img src="img/events/pic%20(4).jpg" alt="">
                        </div>
                        <div class="right-col">
                            <span class="date">26</span>
                            <span class="month">FEB</span>
                            <span class="time">08:00 am</span>
                        </div>
                    </div>
                    <div class="desc">
                        <a href="#">
                            <h3>Abundant Live</h3>
                        </a>
                        <span class="text">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip.
                        </span>
                    </div>
                </div>
                <!-- event item close -->

                <!-- event item begin -->
                <div class="col-md-6 event-item">
                    <div class="inner">
                        <div class="left-col">
                            <img src="img/events/pic%20(5).jpg" alt="">
                        </div>
                        <div class="right-col">
                            <span class="date">01</span>
                            <span class="month">MAR</span>
                            <span class="time">08:00 am</span>
                        </div>
                    </div>
                    <div class="desc">
                        <a href="#">
                            <h3>God is Good</h3>
                        </a>
                        <span class="text">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip.
                        </span>
                    </div>
                </div>
                <!-- event item close -->


                <!-- event item begin -->
                <div class="col-md-6 event-item">
                    <div class="inner">
                        <div class="left-col">
                            <img src="img/events/pic%20(6).jpg" alt="">
                        </div>
                        <div class="right-col">
                            <span class="date">08</span>
                            <span class="month">MAR</span>
                            <span class="time">10:00 am</span>
                        </div>
                    </div>
                    <div class="desc">
                        <a href="#">
                            <h3>Jehovah Jireh</h3>
                        </a>
                        <span class="text">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip.
                        </span>
                    </div>
                </div>
                <!-- event item close -->


            </div>
        </div>

    </div>
    <!-- content close -->

<?php include 'layout/footer.php'; ?>
<?php include 'layout/scripts.php'; ?>