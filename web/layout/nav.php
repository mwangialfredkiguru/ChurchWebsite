<?php 

function ReturnMenuLinks(){
    $BaseURL = "/web/";
    $variable = array(
        ''=>'Home',
        'event.php'=>'Events',
        'sermon.php'=>'Sermons',
        'news.php'=>'News',
        'gallery.php'=>'Gallery',
        'contact.php'=>'Contact'
     );
    foreach ($variable as $key => $value) {
        echo '<li><a href="'.$BaseURL.$key.'">'.$value.'</a></li>';
    }
}
?>

<body>
	<div id="preloader"></div>
    <div id="wrapper">
        <!-- header begin -->
        <header>
            <div class="container">
                <span id="menu-btn"></span>
                <div class="row">
                    <div class="col-md-3">

                        <!-- logo begin -->
                        <div id="logo">
                            <div class="inner">
                                <a href="/">
                                    <img src="img/logo.png" alt="" class="logo-1">
                                </a>
                            </div>
                        </div>
                        <!-- logo close -->
                    </div>

                    <div class="col-md-9">
                        <!-- mainmenu begin -->
                        <div id="mainmenu-container">
                            <ul id="mainmenu">
                                <?php ReturnMenuLinks(); ?>                               
                            </ul>
                        </div>
                        <!-- mainmenu close -->

                        <!-- social icons -->
                        <div class="social">
                            <a href="#"><i class="fa fa-facebook"></i></a>
                            <a href="#"><i class="fa fa-twitter"></i></a>
                            <a href="#"><i class="fa fa-rss"></i></a>
                            <a href="#"><i class="fa fa-google-plus"></i></a>
                            <a href="#"><i class="fa fa-envelope-o"></i></a>
                        </div>
                        <!-- social icons close -->

                    </div>
                </div>
            </div>

        </header>
        <!-- header close -->
