<?php include 'layout/header.php'; ?>
<?php include 'layout/nav.php'; ?>

        <!-- subheader begin -->
        <section id="subheader" data-speed="2" data-type="background">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">

                        <h1>Our Gallery</h1>
                    </div>
                </div>
            </div>
        </section>
        <!-- subheader close -->

        <div class="clearfix"></div>

        <!-- content begin -->
        <div id="content">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <ul id="filters">
                            <li><a href="#" data-filter="*" class="selected">show all</a></li>
                            <li><a href="#" data-filter=".event">event</a></li>
                            <li><a href="#" data-filter=".news">news</a></li>
                            <li><a href="#" data-filter=".gallery">gallery</a></li>
                        </ul>

                    </div>
                </div>
                <div class="row">
                    <div id="gallery-isotope" class="zoom-gallery col-md-12">
                        <div class="item long-pic event">
                            <a class="image-popup-gallery" href="img/gallery/pic%20(1).jpg"><span class="overlay"></span>
                            <img src="img/gallery/pic%20(1).jpg" alt="">
							</a>
                        </div>

                        <div class="item wide-pic news">
                            <a href="img/gallery/pic%20(3).jpg"><span class="overlay"></span></a>
                            <img src="img/gallery/pic%20(3).jpg" alt="">
                        </div>

                        <div class="item small-pic gallery">
                            <a href="img/gallery/pic%20(2).jpg"><span class="overlay"></span></a>
                            <img src="img/gallery/pic%20(2).jpg" alt="">
                        </div>
                        <div class="item small-pic event">
                            <a href="img/gallery/pic%20(5).jpg"><span class="overlay"></span></a>
                            <img src="img/gallery/pic%20(5).jpg" alt="">
                        </div>
                        <div class="item wide-pic news">
                            <a href="img/gallery/pic%20(4).jpg"><span class="overlay"></span></a>
                            <img src="img/gallery/pic%20(4).jpg" alt="">
                        </div>

                        <div class="item small-pic gallery">
                            <a href="img/gallery/pic%20(2).jpg"><span class="overlay"></span></a>
                            <img src="img/gallery/pic%20(2).jpg" alt="">
                        </div>
                        <div class="item wide-pic">
                            <a href="img/gallery/pic%20(3).html""><span class="overlay"></span></a>
                            <img src="img/gallery/pic%20(3).jpg" alt="">
                        </div>


                        <div class="item long-pic news">
                            <a href="img/gallery/pic%20(1).jpg"><span class="overlay"></span></a>
                            <img src="img/gallery/pic%20(1).jpg" alt="">
                        </div>

                        <div class="item wide-pic gallery">
                            <a href="img/gallery/pic%20(4).jpg"><span class="overlay"></span></a>
                            <img src="img/gallery/pic%20(4).jpg" alt="">
                        </div>

                        <div class="item small-pic event">
                            <a href="img/gallery/pic%20(1).jpg"><span class="overlay"></span></a>
                            <img src="img/gallery/pic%20(5).jpg" alt="">
                        </div>



                    </div>
                </div>
            </div>
        </div>
        <!-- content close -->

<?php include 'layout/footer.php'; ?>
<?php include 'layout/scripts.php'; ?>